// d=new Date(year,month,date,hours,minute,second,millisecond);//or
// d=new Date(year,month,date,hours,minute,second);//or
// d=new Date(year,month,date,hours,minute);//or
// d=new Date(year,month,date,hours;//or
// d=new Date(year,month,date);//or
// d=new Date(year,month);//or

//millisecond means 1s=1000ms

d = new Date(2018, 03, 1, 1, 20, 30, 100); //2018 03 1 1 20 30
console.log(d);
console.log(new Date(2018, 03, 1, 1, 20, 30, 1000)); //2018 03 1 1 20 31
console.log(new Date(2018, 03, 1, 1, 20, 30, 10000)); //2018 03 1 1 20 40

console.log(new Date(2018, 03, 1)); //2018 03 1 0 0 0

console.log(new Date());

let date1 = new Date(2018, 03, 1).getTime();
let date2 = new Date(2018, 03, 8).getTime();

console.log(date2 - date1);
// console.log("hello")
