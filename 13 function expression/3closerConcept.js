const secureBooking = function(){
    //closere concept
    //note you moust retun function and the function must use golbal variable
    let passengerCount = 0;
    return function(){
        // 
        passengerCount=passengerCount + 2;
        console.log(`${passengerCount} passengers`)
    };
    // in first call whenpassengerCount is call on time  at that time 
    // initial passengerCount=0
    // finla passengerCount=2

    // in second call
    // initial passengerCount=2
    // finla passengerCount=0

    //from this way we can generate
};

const booker = secureBooking();
booker() 
booker()
booker()
