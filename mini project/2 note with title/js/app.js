//show the nodes on local storage
showNotes();
//function that will add notes starts
function fun1(e) {


  let addTxt=document.querySelector("#addTxt")//is a object
  let addTitle=document.querySelector("#addTitle")
  let notes=window.localStorage.getItem("notes");//is a string
  let obj={
      Txt:addTxt.value,
      Title:addTitle.value
  }


  if(notes==null)//meaning not define
  {
      notesObj=[];//make empty array
  }
  else//meaning define
  {
      notesObj=JSON.parse(notes)//make array in notes note the notes in localStorage is in string but it contain array data
  }

//adding array of object in localstorage(string)
  notesObj.push(obj);
  window.localStorage.setItem("notes",JSON.stringify(notesObj))
  addTxt.value="";//it is done so that when add note is click the thes in that card vanish
  addTitle.value="";

//show notes when it is added
showNotes();

}
//function that will add notes end






//function that will make the notes from the localstorage
function showNotes()
{
    let notes=window.localStorage.getItem("notes");//is a string
    if(notes==null)
    {
        notesObj=[];
    }
    else
    {
        notesObj=JSON.parse(notes)
    }

    html=""
    notesObj.forEach(function(value,index){ 
        html+=`
        <div class="noteCard my-2 mx-2 card" style="width: 18rem;">
        <div class="card-body">
        <h5 class="card-title">Note ${value.Title}</h5>
        <p class="card-text">${value.Txt}</p> 
        <button id="${index}" onclick="deleteNotes(this.id)" class="btn btn-primary">Delete Notes</button>
        </div>
        </div>

        `

       })
       let notesElm=document.querySelector("#notes")
       if(notesObj.length!=0)
       {
           notesElm.innerHTML=html;
       }

}






//function to deletenotes
function deleteNotes(index)
{
    console.log("note",index,"deleted")
    let notes=window.localStorage.getItem("notes");//is a string
    // if(notes==null)
    // {
    //     notesObj=[];
    // }
    // else
    { 
        notesObj=JSON.parse(notes)
    }  

    notesObj.splice(index,1);
    window.localStorage.setItem("notes",JSON.stringify(notesObj))
    showNotes();

    
}
//function for search
function fun2(e)
{   
    let inputVal= search.value
    let noteCard=document.querySelectorAll(".noteCard")
    for(let element of noteCard)
    {

        // console.log(element.querySelector("p").innerHTML);
        if(element.querySelector("p").innerHTML.includes(inputVal))
        {
            element.style.display="block"
        }
        else
        {
            element.style.display="none";
        }
    }

}







//real code
//add event on addnotes
document.querySelector("#addBtn").addEventListener("click", fun1, false)

//add event on serch
let search=document.querySelector("#searchTxt")
search.addEventListener("input",fun2,false)
