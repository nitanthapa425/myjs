let a = 3;

{
  let a = 4;
  a = 6;
  // for different block different memory space is created
  //first it always try to modify its local variable then ints see its parent then grand parent
  console.log(a);
}

console.log(a);
