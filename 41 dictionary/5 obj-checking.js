const obj1 = { name: "nitan" };
const obj2 = { name: "nitan" };

//for normal varialbel such as string number it only see value and datatyp
//but for other such as obj ,array... it see valuy datatye and memor address
console.log(obj1 === obj1); //it is true because value datatype and memory address is same

console.log(obj1 === obj2); //it is false because value datatype are same but ( memory address is different)

//same for array
const arr1 = [1, 2];
const arr2 = [1, 2];
console.log(arr1 === arr1); //true
console.log(arr1 === arr2); //false
