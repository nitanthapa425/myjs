obj = {
  name: "nitan thapa",
  isMarried: false,
  ["age"]: 28,
  //   or [age]:28 here age must be define outside obj like let age=28
  favMovies: ["hobbit", "got"],
  favPerson: {
    n1: "name",
    age: 28,
  },
};

console.log("first**********", obj);
console.log(obj.name);
//or
console.log(obj["name"]);

obj.name = "ram";
// delete obj.name
//or
// delete obj["name"];
delete obj.favPerson.n1;
console.log("second**********", obj);
console.log("third**********", obj["favMovies"]);
console.log("third**********", Object.values(obj));
