let dic = {
  //objct or dictionary can hold variable as well as function
  name: "nitan",
  surname: "thapa",
  age: 25,
  food: ["banana", "apple"], //array
  dic1: { fmovies: "hobbits", fsong: "comeoncomeon trun a radio un" }, //dictionar
  fun1: function fun() {
    return "just check";
  },
};
console.log(dic["name"]);
dic["name"] = "utshab";
console.log(dic["name"]);
console.log(dic["food"]);
console.log(dic.food[0]);
// console.log(dic.fun());**********imp wrrong;
console.log(dic.fun1());

//in dictionary for one key there is only one value
//it means no repetion of key if repeated old is replace by new
dic1 = { name: "nitan", roll: "425", name: "rajaram", name: "harihar" };
console.log(dic1);

//keys()
// (******point is though it is keys() or values() or entries() the list will be arragned in accending if key(only key not values) are in number  )
// in values() and entries() methode also the key are check not values are check

//for number key
let obj = { 1: "name", 10: "nitan", 5: "thapa" };
console.log(Object.keys(obj)); //['1','5','10]
//point to remember they are arranged in asending order with number
//number is converted to string because key is always a string
//ie key is always a string
//and values may be string or number or any
obj = { 1: "name", 10: "nitan", 5: "thapa" };
console.log(Object.keys(obj)); //['1','5','10]
//though 1,10,5 is string it is sorted according to assending order

//for other than number
obj = { c: "name", a: "nitan", b: "thapa" };
console.log(Object.keys(obj)); //['c','a','b']
//it means  if the keys are other than number they are not place according to accending order

//values() methde
obj = { 1: "name", 10: "nitan", 5: "thapa" };
console.log(Object.values(obj)); //[ 'name', 'thapa', 'nitan' ] ...it is arrange according to keys number

obj = { 1: 1, 10: 2, 5: 3 };
console.log(Object.values(obj)); //[ 1,3,2] ...it is arrange according to keys number

obj = { c: "name", a: "nitan", b: "thapa" };
console.log(Object.values(obj)); //[ 'name', 'nitan', 'thapa' ]...not arranced becaus key are other than number

//entries() methde
//this methode give sth array of list of key and value
obj = { 1: "name", 10: "nitan", 5: "thapa" };
console.log(Object.entries(obj)); // [ [ '1', 'name' ], [ '5', 'thapa' ], [ '10', 'nitan' ] ]...it is arrange according to keys number

obj = { 1: "name", 10: "nitan", 5: "thapa" };
console.log(Object.entries(obj)); //[ [ '1', 'name' ], [ '5', 'thapa' ], [ '10', 'nitan' ] ]

obj = { c: "name", a: "nitan", b: "thapa" };
console.log(Object.entries(obj)); // [ [ 'c', 'name' ], [ 'a', 'nitan' ], [ 'b', 'thapa' ] ]...not arranced becaus key are other than number

console.log(00010 - 1); //7 thus it is necessary to handle 0 before the number (in generally)
