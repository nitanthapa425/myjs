// a&&b&&c
// and return first false comming from left to right
//it it does not find false then it will return the last
console.log(1&&2&&3)
//3
//because 1 is true, 2 is true, and 3 is true thus return last 

console.log(true&&1&&false)
//false because it is first false 


console.log(true&&false&&1)
//false because it is the fist false


console.log(true&&true&&null)
//null
//because null is false which is first false

console.log(true&&null&&undefined)
//null

console.log(true&&{}&&undefined&&""&&null)
// exception
// undefined
//because {} is considered as true



// vvvimp concept
// {} is consider as empty and empty is false
// "" is considered as empty and empty is false
// but
// [] is not consired empty

console.log(true&&undefined&&null&&[]&&{})
//undefined because it is first


//general use of and 
// 2>1&&3>2&&(execute this)
//if 2>1 and 3>2 are true then only execute this is execute



