console.log(!!2)//true
// !! convert any thing to boolean value based on truthy or falsy value
console.log(!!0)//false
console.log(!!{})//true
console.log(!![])//true
console.log(!!"")//false
console.log(!!null)//false
console.log(!!undefined)//false
console.log(!!NaN)//false

