//nullish coalescing operator??
//same as ||
// a||b||c||d 
// if a is not present ie if a is any falsy 
// then it check b

// a??b??c??d
// if a is null or undefined the only
// it check b



let baz 

baz= false ||42;
console.log(baz);
// expected output: 42


baz= false ?? 42;
console.log(baz);
// expected output: false


baz= null?? 42;
console.log(baz);
// expected output: 42

baz= undefined?? 31;
console.log(baz);
// expected output: 42
