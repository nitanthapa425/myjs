// const obj={
//     name:"nitan",
//     age:26,
//     fun:()=>{
//         return("i am from return")
//     },
//     arr:[1,2,3]
// }

// //in object 
// // obj.surname  if you call obj.surname it will result undefined   because surname is not present
// // and if you do any operation in undefined it will crash whole program (which must be  prevent it is possilbe by ?.)
// // undefined.length..error
// //undefined.other ....erro

// //but undefined?.other ....produce undefined doesnot produce error 



// console.log(undefined?.ram)
// //gives undefined

// // ?. i called optional chang
// // ?. is only use for object 


// //now just replace . by ?. 
// console.log(obj?.name)
// //.name meand obj.name
// // produce nitan
// console.log(obj?.name?.data)
// //.nmae means obj.nmae
// //.data means obj.name.data
// // produce undefined
// //if any property in object is not find it gives undefined


// // addigional property 1
// //each property is access by ?. even for array and function
// console.log(obj?.arr?.[0])
// //  console.log(obj.arr.[0])  is not possible

// console.log(obj?.arr[0])
// //or you can acces it by

// console.log(obj?.fun())
// console.log(obj?.fun?.())
// // console.log(obj.fun.())....is not possible
// // .() means obj.fun()
// //  expected output i am from return


// console.log(obj?.arr[0])
// //or you can acces it by




// //addigional property 2
// let age="age"
// // console.log(obj.[age])    ...it is not possibel but ?. make it possibel
// // console.log(obj?.[age])
// //26


// // simple main point 
// // only use ?. only in object case
// //  use . in place of ?.
// //and use obj?.arr?.[0]  in place of  obj.arr[0]
// //access each property by ?. even fro array or function
// //if you do this then if any situation occurer where 
// // undefine or null action is try to done
// //it will not through err but will gives undefined
// //simple point
// //undefined?.length gives undefined
// //if ?. is use
// //then it will give undefine if any thing is try to done with undefined

// //or other way but note it is only valid for object (member)
// console.log(undefined??obj.name)     //gives nitan .....it means if left is undefined or null then return obj.name
// console.log(undefined??"sham")     //gives nitan .....it means if left is undefined or null then return sham
// console.log(undefined?.obj.name)     //gives undefined...it menas if left is undefined or null then return undefind else return obj.name
// // console.log(undefined?."raja")     //gives undefined  gives error becajse raja is not object memeber


// //disadvantage
// obj.address="gagalphedi" 
// // console.log(obj)


// // obj?.tole="satgattha"
// // produce error it means assignment is not possible



// let a="name"

// let dic={[a]:"nitan"}
// // note is is possi le

// let c='dic'



// ///vvvimp
// //vvvimpt note [var] is only possible to object memeber or object varaible but not valid for object itself
// console.log(dic?.[a])
// console.log(dic?.['name'])//right
// // console.log(dic?.'name')//wrong
// // console.log(dic.[a])...gives error


const detail={
    name:["nitan","ram"]

}
console.log(detail.name[0])

