

//you can export any thing either class ,function or variable
//component is either function class ,variable


//if you export by using export default then in a module or a file there is only one export default component
//ie you can not export more than one component by using export details

//but if you export by using export then in a moduel you can export two or more component
//ie you can export more than one component by using export 

//class
class Nokia {

    name()
    {
        console.log("nitan is my name")
    }
    age()
    {
        console.log("my age is 25")
    }

}
//function
function fun(a,b)
{    
   let c=a+b;
    console.log(c)
}

//variable
let v="variable"

//exporting by using export 
//syntax
// export {functionname,classname,variablename}
//or
//export{functionname}
//


//note you can  export two or more component by using export 

export {fun}
export {Nokia}
export {v}
//or
// export {fun,Nokia,v}
// export default detail...wrong because you can not export two or more component by export default


