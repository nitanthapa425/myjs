//you can export any thing either class ,function or variable
//component is either function class ,variable

//if you export by using export default then in a module or a file there is only one export default component
//ie you can not export more than one component by using export default

//class
class detail {
  name() {
    console.log("nitan is my name");
  }
  age() {
    console.log("my age is 25");
  }
}
//function
function fun(a, b) {
  let c = a + b;
  console.log(c);
}

//variable
let v = "variable";

//exporting by using export default
//syntax
// export default functionname
// export default classname
// export default variablename
//note you can not export two or more component by using export default
//but you can export two or more component by using export
export default fun;
// export default detail...wrong because you can not export two or more component by export default
